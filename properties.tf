resource "consul_keys" "app_properties" {
  key {
    path  = "myapp/properties"
    value = jsonencode({
      key1 = "value1"
      key2 = "value2"
      key3 = "value3"
    })
  }
}
