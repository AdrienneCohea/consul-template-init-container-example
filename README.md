# Quick start

```
# Simulate having a Consul cluster...

systemd-run --user consul agent -dev
terraform init
terraform apply

# We would use hashicorp/consul-template as our init container

consul-template -once -config=config.hcl
```

# Verify the result
```bash
$ cat /tmp/myapp.properties
key1=value1
key2=value2
key3=value3
```
