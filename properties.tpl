{{ with $properties := key "myapp/properties" | parseJSON -}}
{{ range $key, $value := $properties }}{{ $key }}={{ $value }}
{{ end -}}
{{ end -}}
