template {
  # The template is likely stored as a ConfigMap, but there are many ways to slice it.
  source      = "./properties.tpl"

  # Write the resulting file to an emptyDir volume, which will be mounted in the main container
  destination = "/tmp/myapp.properties"
}

log_level = "info"
